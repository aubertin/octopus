#include <stdio.h>
#include <stdlib.h>
#include <uuid.h>

int 
main()
{
        uuid_t          uuid;
        uint32_t        status;
        char*           str[UUID_BUF_LEN];
        uuid_create(&uuid, &status);
        uuid_to_string(&uuid, str, &status);
        printf("%s\n", *str);
        return status;
}
