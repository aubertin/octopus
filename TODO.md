# TODO

## Global

- Port to ARM (using NEON in place of SSE)

- GCC 9: test flags before ?
  
  ```/usr/lib/gcc/x86_64-linux-gnu/9/include/mwaitxintrin.h:41:1: error: '-mindirect-branch' and '-fcf-protection' are not compatible```

## Spectre_v2 

- Use `size_t` instead of `char*` for _target_addr_

## Makefile

- `$REVISION`: use `cat .git/refs/heads/main` instead of `git rev-short`:
  
  ```
  fatal: unsafe repository ('/tmp/octopus' is owned by someone else)
  To add an exception for this directory, call:
    git config --global --add safe.directory /tmp/octopus
  fatal: unsafe repository ('/tmp/octopus' is owned by someone else)
  To add an exception for this directory, call:
    git config --global --add safe.directory /tmp/octopus
  ```
