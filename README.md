OCTOPUS
---

Samuel AUBERTIN - EURECOM - 2022

![SPECTRE Octopus Logo](logo.png "SPECTRE Octopus Logo")

**OCTOPUS** is a [Spectre](https://spectreattack.com/spectre.pdf) V1 (_Bounds Check Bypass_) + V2 (_Branch Target Injection_) vulnerabilty and mitigation tester.

It measures and uploads the success rate of the Spectre attacks on your CPU using different compilers:

- [GCC](https://gcc.gnu.org/)
- [Clang](https://clang.llvm.org/)

... compilation/linking flags such as:

- Optimisation levels (```-O```).
- Static/dynamic linking.

... and mitigations:

- [Masking](https://github.com/torvalds/linux/blob/cb6416592bc2a8b731dabcec0d63cda270764fc6/arch/x86/include/asm/barrier.h#L27) and [lfence](https://www.intel.com/content/www/us/en/developer/articles/troubleshooting/using-intel-compilers-to-mitigate-speculative-execution-side-channel-issues.html) against Spectre V1.
- [RETPOLINE](https://support.google.com/faqs/answer/7625886) against Spectre V2.

## Supported platforms 

- x86_64 with SSE

## Supported distributions

- _OpenBSD 7.0_ 
- _Alpine 3.15_
- _Fedora 35_
- _RHEL 8_
- _Ubuntu 20.04_

## Source support 

Experiment is the most complete when compiled from source:

### OpenBSD

```
$ doas pkg_add git gmake clang gcc
$ git clone https://gitlab.eurecom.fr/aubertin/octopus.git
$ gmake -C octopus all
```

### Alpine

```
$ apk add make git openssh-client-default util-linux-misc util-linux-dev gcc clang musl-dev lld coreutils
$ git clone https://gitlab.eurecom.fr/aubertin/octopus.git
$ make -C octopus all
```

### Fedora/RHEL

```
$ sudo yum install -y git make clang gcc glibc-static lld uuid
$ git clone https://gitlab.eurecom.fr/aubertin/octopus.git
$ make -C octopus all
```

### Debian/Ubuntu
```
$ sudo apt install -y git make gcc clang lld uuid
$ git clone https://gitlab.eurecom.fr/aubertin/octopus.git
$ make -C octopus all
```

## Docker support 

A _Dockerfile_ is provided and used by ```make docker``` but be wary: the compiled binaries inside the docker image are heavily dependent on the CPU platform/version and _not_ portable.

You **have** to build the image on the actual CPU you want to test, otherwise you may stumble upon ```core dumped: illegal instruction``` errors.

## WSL support 

The code runs on _WSL2_ using the default distribution (Ubuntu).

To install WSL, from a priviledged PowerShell prompt:

```
PS C:\Users\user> wsl --install
```

Once the installation is complete, you can install the dependencies and run the experiment from the WSL console:
```
$ sudo apt update && sudo apt upgrade -y && sudo apt install -y git make gcc clang lld uuid
$ git clone https://gitlab.eurecom.fr/aubertin/octopus.git
$ make -C octopus all
```

## Results

Results are stored as JSON objects in an unique file for each run, following this pattern: ```result-$(UUID).json```

## Dependencies

- Compilers: ```clang``` and ```gcc```
- Linker: ```lld```
- For upload: ```openssh``` and ```uuid```
- The C library static symbols, on Fedora: ```glibc-static```

## Running the experiment with `Make`

```make``` will build, execute and upload the experiment results.

### PoC only 

```make poc``` will compile and execute v1 and v2 executables once using clang and no optimisations.

### Within docker

```make docker``` will build, execute and upload the experiment results within Docker.

```make dockerbuild``` will only build the experiment images named _octopus_.

```make dockerpoc``` will run the PoC within Docker.

### Build only 

```make build``` will only build the experiment binaries.

### Cleanup

```make clean``` will remove experiment binaries and result files.

```make dockerclean``` will remove the _octopus_ image.

## Common errors 

- ```core dumped: illegal instruction```: Either your CPU does not support _SSE_ or you compiled the executables on a different CPU than the one you are testing.

## Results aggregation and upload

Results are automatically uploaded to a server with a dedicated account using ```sftp```.

Here is an exhaustive list of the data sent:
- CPU model name and microcode version.
- Kernel version and compilation date.
- GCC and clang versions.
- The list of mitigations enabled at runtime.
- The cache timings of the processor computed by the ```calibrate_threshold()``` function and the experiment duration.
- Each spectre execution success rate.
- The code version.

**NONE** of this data will be used for anyhting else except this experiment.

## References

- [Spectre Attacks: Exploiting Speculative Execution](https://spectreattack.com/spectre.pdf)

Both implementations were inspired by:

- [ErikAugust gist](https://gist.github.com/ErikAugust/724d4a969fb2c6ae1bbd7b2a9e3d4bb6)
- [genua GmbH PoC](https://github.com/genua/meltdown)
