FROM alpine
RUN apk update
# Copy everything, except executables listed in .dockerignore
COPY . /octopus
WORKDIR /octopus
# Install runtime dependencies
RUN apk add --no-cache make git openssh-client-default util-linux-misc util-linux-dev
# Install build dependencies, compile and uninstall dependencies
RUN apk add --no-cache --virtual build-dependencies gcc clang musl-dev lld coreutils \
	&& make -j$(nproc) build \
	&& apk del build-dependencies
# Run all experiment per default
CMD make all
